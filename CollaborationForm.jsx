import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Form, Field, reduxForm } from "redux-form";
import { required } from "utils/form-validation.js";
import CollaborationTable from "pages/collaboration/collaboration/CollaborationTable";
import Input from "components/common/input/Input";
import Select from "components/common/select/Select";
import TextArea from "components/common/textarea/textArea";

class CollaborationForm extends Component {
  state = { isChecked: true };
  clickCheckbox() {
    if (this.state.isChecked === true) {
      document.getElementById("mySelect").disabled = false;
      this.setState({ isChecked: !this.state.isChecked });
    } else {
      document.getElementById("mySelect").disabled = true;
      this.setState({ isChecked: !this.state.isChecked });
    }
  }

  handleSubmit(){
    console.log("saved")
  }
  constructor() {
    super();
    this.clickCheckbox = this.clickCheckbox.bind(this);
  }
  render() {
    const { handleSubmit, history } = this.props;
  return (
    <div>
      <div className="layout-style">
        <div>
          <h1>
            Collaboration<sub>[Add New] or [Edit]</sub>
          </h1>
        </div>
        <div>
        <button
              className="btn btn-danger btn-sm m-2"
              onClick={() => history.goBack()}
              style={{ width: "100px", marginRight: "10px" }}
            >
              Cancel
            </button>
          <button
            className="btn button-style btn-success btn-sm m-2"
            type="submit"
          >
            Save
          </button>
        </div>
      </div>
      <div className=" col-xl-10 col-10 col-sm-10 col-md-10 col-lg-10 m-3 p-3 background-style"
      style={{ backgroundColor: "white", borderTop: "#17a2b8 3px solid" }}
      >
        <h6>Information</h6>
        <hr />
        <Form onSubmit={handleSubmit}>
            <Field
              classInput="form-control"
              name="select"
              component={Select}
              options={["BTE", "BTT"]}
              type="select"
              placeholder="Collaboration Type"
              id="select-collaboration-type"
              validate={[required]}
            />
            <Field
              classInput="form-control"
              name="select"
              component={Select}
              options={["Telkomsel", "Astra", "Kumparan", "Tech in Asia"]}
              type="select"
              placeholder="Select Corporate"
              label="Select Corporate"
              id="select-corporate"
              validate={[required]}
            />
            {/* <Field
              type="checkbox"
              name="checkbox"
              component={Input}
              value="Collaboration for Event"
              onChange={this.clickCheckbox}
              label="Collaboration for Event"
            /> */}
            <Field
              classInput="form-control"
              name="eventname"
              component={Select}
              type="select"
              placeholder="Event Name"
              label="Event Name"
              options={["lol", "lel", "lalala"]}
              validate={[required]}
            />
            <Field
              classInput="form-control"
              name="email"
              component={Input}
              type="email"
              placeholder="Email"
              label="Email"
              id="input-email"
              validate={[required]}
            />
            <Field
              classInput="form-control"
              name="phone"
              component={Input}
              type="text"
              placeholder="Phone Number"
              label="Phone Number"
              id="input-phone-number"
              validate={[required]}
            />
        </Form>
      </div>
    </div>
  );
};

export default reduxForm({ form: "CollaborationForm" })(CollaborationForm);